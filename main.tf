terraform {
  required_version = ">= 0.13.2"
}

locals {
  application_name  = "pma"
}

data "aws_ecs_cluster" "ecs_cluster" {
  cluster_name = var.cluster_name
}

resource "aws_service_discovery_service" "discovery_service" {
  name = local.application_name

  dns_config {
    namespace_id = var.namespace_id

    dns_records {
      ttl  = 60
      type = "A"
    }

    dns_records {
      ttl  = 60
      type = "AAAA"
    }

    routing_policy = "WEIGHTED"
  }

  health_check_config {
    failure_threshold = 1
    resource_path     = "/"
    type              = "HTTP"
  }
}

resource "aws_cloudformation_stack" "pma" {
  name                    = "${var.project_name}-${local.application_name}-${var.environment}"

  parameters    = {
    ProjectName           = "${var.project_name}-${var.environment}"
    ClusterArn            = data.aws_ecs_cluster.ecs_cluster.arn
    LogGroupName          = var.log_group
    Subnets               = join(",", var.subnet_ids)
    CloudMapNamespaceName = var.namespace_name
    SecurityGroup         = var.security_group
    TaskExecutionRoleARN  = var.task_execution_role_arn
    MysqlHost             = var.mysql_host
    Cpu                   = tostring(var.cpu)
    Memory                = tostring(var.memory)
  }

  capabilities  = ["CAPABILITY_IAM", "CAPABILITY_NAMED_IAM", "CAPABILITY_AUTO_EXPAND"]
  on_failure    = "DELETE"

  template_body = file("${path.module}/stack.json")
}
