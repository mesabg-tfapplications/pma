output "dns_name" {
  value       = "pma.${var.namespace_name}"
  description = "Resource DNS name"
}
