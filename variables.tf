variable "environment" {
  type        = string
  description = "Environment name"
}

variable "project_name" {
  type        = string
  description = "General project name"
}

variable "cluster_name" {
  type        = string
  description = "Cluster name"
}

variable "subnet_ids" {
  type        = list(string)
  description = "Subnet identifiers"
}

variable "security_group" {
  type        = string
  description = "Security Group to assign"
}

variable "task_execution_role_arn" {
  type        = string
  description = "TaskExecution role for application"
}

variable "mysql_host" {
  type        = string
  description = "MySQL Host domain"
}

variable "log_group" {
  type        = string
  description = "Log Group Name"
}

variable "namespace_id" {
  type        = string
  description = "CloudMap namespace identifier"
}

variable "namespace_name" {
  type        = string
  description = "CloudMap namespace name"
}

variable "cpu" {
  type        = number
  description = "Number of CPU units to assign to the proccess"
  default     = 256
}

variable "memory" {
  type        = number
  description = "Amount of memory to assign to the proccess"
  default     = 512
}
